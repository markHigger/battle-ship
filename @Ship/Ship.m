% REVISE: move this script out of @ship folder into the main folder
classdef Ship < handle
    % Ship manages the placement a ship and manages which of its
    % coordinates has been hit
    properties
        Type            % string, type of ship
        Placement       % [size x 2], where each row is a coordinate the
        %               ship occupies
        LivePlacement   % [numLive x 2], where each row is a coordinate the
        %               ship occupies (which hasn't been hit yet)
    end
    properties (Dependent)
        Size            % REVISE: comment needed
        Sunk=false      % REVISE: comment needed, also, default not needed on dependant property
    end
    
    methods
        
        function self = Ship(type) % NOTE: even handle objects return self, but only in constructor
            self.Type = type;
        end
        
        function Size=get.Size(self) % REVISE: new name, don't use matlab built in names (I wrote this comment last time!)
            Size=size(self.Placement, 1);
        end
        
        function sunk=get.Sunk(self)
            if self.LivePlacement==0    % REVISE: LivePlacement is an array, what is array == 0 (we talked about this last time, use any)
                fprintf('%s is sunk \n', self.Type);
                sunk=true;
            else
                sunk=false;
            end
        end
        
        function SetPosition(self) % NOTE: because ship is a handle object, there in no need to return self
            % REVISE: a terse description of function goes here
            %Input
            % REVISE: this function has no inputs
            %   User is prompted to type in the starting and ending
            %   coordinates in x,y formatt
            %     **End point must be in line with start point on either th
            %     **x or y direction. all points also must be within a
            %     **10x10 square
            %Output
            % REVISE: this function has no outputs (it sets internal property)
            %   selfect placement in a nx2 format matrix
            
            firstCoord(1) = input('Input First x Coordinate: ');
            firstCoord(2) = input('Input First y Coordinate: ');
            lastCoord(1)  = input('Input Last X Coordinate: ');
            lastCoord(2)  = input('input Last Y Coordinate: ');
            
            % check that ship is in single row or col
            if sum(logical(firstCoord - lastCoord)) == 2
                warning('ship must be in single row or col, please re-enter');
                self.SetPosition();   % NOTE: handle objects ...
                return
            end
            
            % check that ship in in board bounds
            MINCOORD = 1;
            MAXCOORD = 10;
            coordInvalid = @(coord)(max(coord) > MAXCOORD || min(coord) < MINCOORD);
            if coordInvalid(firstCoord) || coordInvalid(lastCoord)
                warning(['ship must be between ', num2str(MINCOORD), ...
                    ' and ', num2str(MAXCOORD), ' please re-enter']);
                self.SetPosition();
                return
            end
                
            % delta is the vector which points from first to lastCoord (of
            % length 1)
            delta = logical(lastCoord - firstCoord);
            
            % add a row to Placement for each space teh ship occupies,
            % starting from firstCoord until lastCoord is reached
            self.Placement(1, :) = firstCoord;
            while ~isequal(self.Placement(end, :), lastCoord)
                self.Placement(end+1, :) = self.Placement(end, :) + delta;
            end

            self.LivePlacement=self.Placement;
        end        
        
        function CheckIfHit(self,x,y)   % NOTE: because ship is a handle object, there in no need to return self
        %Checks if input coordinates hit the given ship
        %
        %This method detects if an input coordinate matches any of the
        %ships live placement coordinates
        %
        %Inputs
        %   self is the inputed ship % REVISE: remove this line, the first input to each handle object method is always self, no need to repeat that with each method
        %   x = scalar integer, x coordinate
        %   y = scalar integer, y coordinate
                    
            for idx=1:self.Size
                if isequal(self.LivePlacement(idx,:), [x, y])
                    disp('Hit!');
                    self.LivePlacement(idx,1:2)=[0, 0];
                    if self.LivePlacement==0    % REVISE: use any! livePlacement == 0 returns an array, this works but its awkward
                        fprintf('You Sunk a %s \n', self.Type);
                    end
                end
            end
        end
    end
end