classdef BattleShip < handle
    properties
        Ships   % revise: comments?
        Win = false
    end
    
    methods
        function self=BattleShip(ships)
            % Input:
            %   Ships is a vector of Ship Class selfects
            
            % REVISE: its unclear if ships is set outside the constructor
            % or inside it, maybe you mean to pass numShips to the
            % constructor and then loop through and ask the user to set
            % their position below?
            
            self.Ships=ships;
            for i=1:length(self.Ships)  % REVISE: don't use i as an idx, use idx or ii (for nested loops, consider ii, jj, kk, ll etc)
                fprintf('Ship %d Coordinates \n',i)
                self.Ships(i)=SetPosition(self.Ships(i));
            end
        end
        
        function FireAt(self,x,y)
            %Checks if user input coordinats are at any of the ships 
            % Inputs
            %   x = scalar integer to be queried
            %   y = scalar integer to be queried
            %   REVISE: accept a vector [x, y] rather than two scalars, do
            %   this in Ship.CheckIfHit as well
            
            %goes through a loop to see if the coordinates match with the live
            %position of any of the ships
            % NOTE: this comment above may be too obvious, is it needed?
            % What questions would a reader of the code below still have?
            % Because you have picked such descriptive names for your
            % objects and methods, the code speaks for itself!  :)
            for i= 1:length(self.Ships)
                self.Ships(i)=CheckIfHit(self.Ships(i),x,y);
            end
        end
        
        
        function win=get.Win(self)
            % evaluates whether all ships have been sunk
                        
            win = all([self.Ships(1:length(self.Ships)).Sunk]);
        end
    end
end


