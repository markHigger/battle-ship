%This is th game 'BattleShip'
%To Play BattleShip simply Run The file
%When prompted enter in the starting and ending coordinates for each Ship
%

clc;
clearvars;
InitialConditions;

Games=BattleShip(Ships);

while Games.Win==false
    x=input('x: ');
    y=input('y: ');
    Games.FireAt(x,y);
    fprintf('\n \n');
end