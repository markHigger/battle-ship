
%This Function sets up the standard initial conditions of BattleShip
%   This function sets up the inate conditions of the ships
%
%Output
%   This This gives out the inital ships in an array, where each ship is
%       the Ship class
%   The Array order of the ships is as follows;
%       Ships(1)= Patrol Boat, Ships(2) = Cruiser, Ships(3)= Submarine
%       Ships(4)= BattleShip, Ships(5)= Aircraft Carriere

% REVISE: remove the above comments, this isn't a function.  Because we're
% combining Game and GameTest into BattleShipTest, it might make sense to
% put this in there as well (it doesn't need to be called in two palces
% anymore, does it?)

Ships(1)=Ship('Patrol Boat');
Ships(2)=Ship('Cruiser');
Ships(3)=Ship('Submarine');
Ships(4)=Ship('Battleship');
Ships(5)=Ship('Aircraft Carrier');



